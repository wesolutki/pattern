#include <rili/Test.hpp>
#include <rili/TypeId.hpp>
#include <utility>

TEST(TypeId, ComparatorReturnTrueForExactlyTheSameTypes) {
    struct A {};
    struct B;

    EXPECT_EQ(rili::TypeId::create<int>(), rili::TypeId::create<int>());
    EXPECT_EQ(rili::TypeId::create<double>(), rili::TypeId::create<double>());
    EXPECT_EQ(rili::TypeId::create<A>(), rili::TypeId::create<A>());
    EXPECT_EQ(rili::TypeId::create<B>(), rili::TypeId::create<B>());
    EXPECT_EQ((rili::TypeId::create<std::pair<int, int>>()), (rili::TypeId::create<std::pair<int, int>>()));

    typedef A A1;
    typedef B B1;
    typedef std::pair<int, int> Pair;

    EXPECT_EQ(rili::TypeId::create<A>(), rili::TypeId::create<A1>());
    EXPECT_EQ(rili::TypeId::create<B>(), rili::TypeId::create<B1>());
    EXPECT_EQ((rili::TypeId::create<std::pair<int, int>>()), (rili::TypeId::create<Pair>()));
}

TEST(TypeId, ComparatorReturnFalseForDifferentTypes) {
    struct A {};
    struct B;

    EXPECT_NE(rili::TypeId::create<int>(), rili::TypeId::create<double>());
    EXPECT_NE(rili::TypeId::create<int>(), rili::TypeId::create<A>());
    EXPECT_NE(rili::TypeId::create<int>(), rili::TypeId::create<B>());
    EXPECT_NE(rili::TypeId::create<int>(), (rili::TypeId::create<std::pair<int, int>>()));

    EXPECT_NE(rili::TypeId::create<double>(), rili::TypeId::create<A>());
    EXPECT_NE(rili::TypeId::create<double>(), rili::TypeId::create<B>());
    EXPECT_NE(rili::TypeId::create<double>(), (rili::TypeId::create<std::pair<int, int>>()));

    EXPECT_NE(rili::TypeId::create<A>(), rili::TypeId::create<B>());
    EXPECT_NE(rili::TypeId::create<A>(), (rili::TypeId::create<std::pair<int, int>>()));

    EXPECT_NE(rili::TypeId::create<B>(), (rili::TypeId::create<std::pair<int, int>>()));
}

TEST(TypeId, ComparatorReturnFalseForDifferentButSimmilarTypes) {
    struct A1 {};
    struct A2 {};

    struct B1;
    struct B2;

    struct C1 {
        int a;
    };

    struct C2 {
        int a;
    };

    EXPECT_NE(rili::TypeId::create<int>(), rili::TypeId::create<unsigned int>());
    EXPECT_NE(rili::TypeId::create<A1>(), rili::TypeId::create<A2>());
    EXPECT_NE(rili::TypeId::create<B1>(), rili::TypeId::create<B2>());
    EXPECT_NE(rili::TypeId::create<C1>(), rili::TypeId::create<C2>());

    struct A3 : public A2 {};

    EXPECT_NE(rili::TypeId::create<A2>(), rili::TypeId::create<A3>());

    // feed cppcheck
    C1 c1{0};
    C2 c2{0};
    EXPECT_EQ(c1.a, c2.a);
}
