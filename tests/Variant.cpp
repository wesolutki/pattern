#include <memory>
#include <rili/Test.hpp>
#include <rili/Variant.hpp>

TEST(Variant, WhenConstructedThenUninitialized) {
    rili::Variant<bool, int, std::shared_ptr<int>> variant;

    EXPECT_FALSE(variant.valid());
    EXPECT_FALSE(variant.is<bool>());
    EXPECT_FALSE(variant.is<int>());
    EXPECT_FALSE(variant.is<std::shared_ptr<int>>());

    EXPECT_THROW(std::bad_cast, [&variant]() { variant.get<bool>(); });
    EXPECT_THROW(std::bad_cast, [&variant]() { variant.get<int>(); });
    EXPECT_THROW(std::bad_cast, [&variant]() { variant.get<std::shared_ptr<int>>(); });
}

TEST(Variant, WhenSetThenValidAndAccesiable) {
    rili::Variant<bool, int, std::shared_ptr<int>> variant;

    variant.set<bool>(true);

    EXPECT_TRUE(variant.valid());
    EXPECT_TRUE(variant.is<bool>());
    EXPECT_FALSE(variant.is<int>());
    EXPECT_FALSE(variant.is<std::shared_ptr<int>>());

    variant.get<bool>();
    EXPECT_THROW(std::bad_cast, [&variant]() { variant.get<int>(); });
    EXPECT_THROW(std::bad_cast, [&variant]() { variant.get<std::shared_ptr<int>>(); });
}

TEST(Variant, WhenSetThenValuePreservedToNextSet) {
    rili::Variant<bool, int, std::shared_ptr<int>> variant;
    std::weak_ptr<int> w;
    {
        auto s = std::make_shared<int>(5);
        variant.set<std::shared_ptr<int>>(s);
        w = s;
    }
    EXPECT_EQ(w.use_count(), 1);
    {
        auto s = variant.get<std::shared_ptr<int>>();
        EXPECT_EQ(*s, 5);
    }
    EXPECT_EQ(w.use_count(), 1);
    {
        variant.set<std::shared_ptr<int>>(std::make_shared<int>(10));  // NOLINT
    }
    EXPECT_EQ(w.use_count(), 0);
}
