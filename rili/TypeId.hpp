#pragma once
/** @file */

namespace rili {

/**
 * @brief The TypeId class can be used as very lightweight replacement for RTTI.
 *
 * Normally when RTTI is used compiler will generate type information for every possible type in program - this can
 * introduce huge binary bloat. TypeId class in oposition allow you to compare types in runtime only with marginal
 * overhead and only for types which are really needed. This technique is well known and used in many serious projects -
 * for example in LLVM. Most important trick is `create()` method, rest is just syntactic sugar.
 */
class TypeId {
 public:
    /**
     * @brief create rili::TypeId instance for given, known in compile time type.
     * @return TypeId object instance for given in template Parameter type.
     */
    template <typename T>
    TypeId static create() noexcept {
        static int instance;
        return TypeId(reinterpret_cast<void const*>(&instance));
    }

    /**
     * @brief copy constructor
     *
     * @param other TypeId object
     */
    TypeId(TypeId const& other) = default;

    /**
     * @brief copy assigment operator
     *
     * @param other TypeId object
     * @return reference pointing to current TypeId instance
     */
    TypeId& operator=(TypeId const& other) = default;

    /**
     * @brief compare operator
     *
     * @param other TypeId object
     * @return true if other was created for the same type, otherwise false
     */
    inline bool operator==(TypeId const& other) const { return m_id == other.m_id; }

    /**
     * @brief compare operator
     *
     * @param other TypeId object
     * @return false if other was created for the same type, otherwise true
     */
    inline bool operator!=(TypeId const& other) const { return m_id != other.m_id; }

    /**
     * @brief TypeId default constructor constructs invalid TypeId (no real type can have equal TypeId)
     */
    inline TypeId() : m_id(nullptr) {}

 private:
    explicit inline TypeId(void const* id) : m_id(id) {}

 private:
    const void* m_id;
};
}  // namespace rili
