#pragma once
/** @file */

#include <algorithm>
#include <rili/TypeId.hpp>
#include <string>
#include <type_traits>
#include <typeinfo>
#include <utility>

namespace rili {
namespace detail {
namespace variant {
template <typename... Ts>
struct MaxTypeInfo;

template <typename T1, typename... Ts>
struct MaxTypeInfo<T1, Ts...> {
    static constexpr std::size_t align = alignof(T1) > MaxTypeInfo<Ts...>::align ? alignof(T1)
                                                                                 : MaxTypeInfo<Ts...>::align;
    static constexpr std::size_t size = sizeof(T1) > MaxTypeInfo<Ts...>::size ? sizeof(T1) : MaxTypeInfo<Ts...>::size;
};

template <typename T1>
struct MaxTypeInfo<T1> {
    static constexpr std::size_t align = alignof(T1);
    static constexpr std::size_t size = sizeof(T1);
};

template <class... Types>
struct aligned_union {
    struct type {
        alignas(MaxTypeInfo<Types...>::align) char _s[MaxTypeInfo<Types...>::size];
    };
};

template <typename...>
struct IsOneOf {
    static constexpr bool value = false;
};

template <typename T, typename S, typename... Types>
struct IsOneOf<T, S, Types...> {
    static constexpr bool value = std::is_same<T, S>::value || IsOneOf<T, Types...>::value;
};

template <typename... Others>
struct Helper;

template <typename T, typename... Others>
struct Helper<T, Others...> {
    inline static void destroy(TypeId id, void* data) {
        if (id == TypeId::create<T>())
            reinterpret_cast<T*>(data)->~T();
        else
            Helper<Others...>::destroy(id, data);
    }

    inline static void move(TypeId id, void* other, void* self) {
        if (id == TypeId::create<T>())
            new (self) T(std::move(*reinterpret_cast<T*>(other)));
        else
            Helper<Others...>::move(id, other, self);
    }

    inline static void copy(TypeId id, const void* other, void* self) {
        if (id == TypeId::create<T>())
            new (self) T(*reinterpret_cast<const T*>(other));
        else
            Helper<Others...>::copy(id, other, self);
    }
};

template <>
struct Helper<> {
    inline static void destroy(TypeId, void*) {}

    inline static void move(TypeId, void*, void*) {}

    inline static void copy(TypeId, const void*, void*) {}
};
}  // namespace variant
}  // namespace detail

/**
 * @brief Variant is data structure which can be used like union but allow to store non-POD types
 */
template <typename... Types>
struct Variant {
 private:
    typedef typename detail::variant::aligned_union<Types...>::type Data;
    typedef detail::variant::Helper<Types...> Helper;

 public:
    /**
     * @brief Variant initialize Variant with invalid value
     */
    Variant() : m_id(TypeId()) {}

    /**
     * @brief Variant copy constructor
     * @param other other Variant
     */
    Variant(const Variant<Types...>& other) : m_id(other.m_id) { Helper::copy(other.m_id, &other.m_data, &m_data); }

    /**
     * @brief Variant move constructor
     * @param other other Variant
     */
    Variant(Variant<Types...>&& other) : m_id(other.m_id) { Helper::copy(other.m_id, &other.m_data, &m_data); }

    /**
     * @brief operator = is both Variant copy and move assignment operator
     * @param other other Variant
     * @return
     */
    Variant<Types...>& operator=(Variant<Types...> other) {
        std::swap(m_id, other.type_id);
        std::swap(m_data, other.m_data);
        return *this;
    }

    /**
     * @brief Check if currently stored value in Variant is given T type.
     * @return true is types match, false otherwise
     */
    template <typename T>
    bool is() const {
        static_assert(detail::variant::IsOneOf<T, Types...>::value, "Having this type in this Variant is not possible");
        return m_id == TypeId::create<T>();
    }

    /**
     * @brief valid check if Variant is correctly initialized
     * @return true is correctly initialized, false otherwise
     */
    bool valid() { return m_id != TypeId(); }

    /**
     * @brief used to set value of Variant and change type if needed
     * @param args value to be set
     */
    template <typename T, typename... Args>
    void set(Args&&... args) {
        static_assert(detail::variant::IsOneOf<T, Types...>::value,
                      "You can not set value of given type to this Variant");
        Helper::destroy(m_id, &m_data);
        new (&m_data) T(std::forward<Args>(args)...);
        m_id = TypeId::create<T>();
    }

    /**
     * @brief used to get value of Variant
     *
     * @warning if type T is not equal to currently stored type in Variant std::bad_cast will be thrown
     * @return current Variant value
     */
    template <typename T>
    T& get() {
        static_assert(detail::variant::IsOneOf<T, Types...>::value,
                      "You can not gat value of given type from this Variant");
        if (m_id != TypeId::create<T>()) {
            throw std::bad_cast();
        }
        return *reinterpret_cast<T*>(&m_data);
    }

    /**
     * @brief used to get value of Variant
     * @return current Variant value
     *
     * @warning if type T is not equal to currently stored type in Variant std::bad_cast will be thrown
     */
    template <typename T>
    T const& get() const {
        static_assert(detail::variant::IsOneOf<T, Types...>::value,
                      "You can not gat value of given type from this Variant");
        if (m_id != TypeId::create<T>()) {
            throw std::bad_cast();
        }
        return *reinterpret_cast<T const*>(&m_data);
    }

    ~Variant() { Helper::destroy(m_id, &m_data); }

 private:
    TypeId m_id;
    Data m_data;
};
}  // namespace rili
